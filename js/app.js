App = Ember.Application.create();

App.Router.map(function() {
  this.route('about', { path:'/about'});
});

App.Cells = [];

App.IndexRoute = Ember.Route.extend({
	model: function() {
		return App.Cells;	
    }	
});

App.IndexController = Ember.Controller.extend({
	init: function(){
		this.newGame();	
	},

	actions: {
		move: function(e){
			var number = parseInt($(e.target).text()),//получаем номер фишки
			index = this.model.indexOf(number),//получаем  индекс соотв. элемента массива (модели)
			indexEmptyCell = this.model.indexOf(0);//получаем индекс нулевого элемента массива (модели)
			
			if(this.checkMoveAbil(index, indexEmptyCell)){
				this.model.removeAt(index);
				this.model.insertAt(index, 0);
				this.model.removeAt(indexEmptyCell);
				this.model.insertAt(indexEmptyCell, number);				
			}
			this.checkModel();			
		},
		
		newGame: function (){
			this.newGame();
		},	
	},
	//проверка возможности передвинуть фишку
	checkMoveAbil: function (index, indexEmptyCell) {
		var difference = Math.abs(index - indexEmptyCell); 
				 
		if (difference == 4) {
			return true;
		} else if (difference == 1) {
			if((index == 3 && indexEmptyCell == 4) || 
				(index == 4 && indexEmptyCell == 3) ||
				(index == 7 && indexEmptyCell == 8) ||
				(index == 8 && indexEmptyCell == 7) ||
				(index == 11 && indexEmptyCell == 12) ||
				(index == 12 && indexEmptyCell == 11)){
				return false;
			} else {
				return true;
			}
		} 		
	},
	//проверка соответствия порядка фишек
	checkModel: function (){
		for(var i = 0; i < 15; i++){
			if (this.model.get(i) != i + 1) break;
		}
		if(i == 15) $('#win').css('display', 'block');
	},

	newGame: function(){
		$('#win').css('display', 'none');
		App.Cells.length = 0;//todo: 
		while(App.Cells.length < 16){
			var number = Math.floor(Math.random() * 16);
			if (App.Cells.indexOf(number) == -1)
				App.Cells.pushObject(number);			
		}					
	}	
});

App.AboutRoute = Ember.Route.extend({
  model: function() {
	return '';
  }
});